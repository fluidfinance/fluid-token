// SPDX-License-Identifier: MIT
pragma solidity 0.8.7;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/draft-ERC20PermitUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlEnumerableUpgradeable.sol";

interface IrFLUID {
  function onRegister(address to_, uint256 amount_) external;
}

/// @title Wrapped Fluid Shares
/// @author Fluid Finance SA
/// @notice Wrapped registered equity shares in the Fluid Finance SA
contract FLUID is ERC20Upgradeable, ERC20PermitUpgradeable, AccessControlEnumerableUpgradeable {

  uint8 internal constant DECIMALS_VALUE = 18;

  IrFLUID public rFLUID;
  
  event UnwrappedFLUID(address indexed account, uint256 amount);
  event WrappedFLUID(address indexed account, uint256 amount);
  
  modifier onlyRFLUID {
    require(msg.sender == address(rFLUID), 'caller is not rFLUID');
    _;
  }
  
  function initialize(
    string calldata name_, 
    string calldata symbol_,
    address admin_,
    address rFLUID_
  ) public initializer {
    require(admin_ != address(0), "admin_ is address zero");
    require(rFLUID_ != address(0), "rFLUID_ is address zero");
    
    ERC20Upgradeable.__ERC20_init(name_, symbol_);
    ERC20PermitUpgradeable.__ERC20Permit_init(name_);
    AccessControlEnumerableUpgradeable.__AccessControlEnumerable_init();

    // can (re)assign any of the roles
    _setupRole(DEFAULT_ADMIN_ROLE, admin_);
    
    rFLUID = IrFLUID(rFLUID_);
  }
  
  function onWrap(
    address recipient_,
    uint256 amount_
  ) external onlyRFLUID {
    require(recipient_ != address(0), "cannot wrap to address zero");
    require(amount_ > 0, "cannot wrap zero tokens");
    // rFLUID has 0 decimals, FLUID has 18 decimals, but that is already 
    // taken care of in rFLUID.wrap
    _mint(recipient_, amount_);
    emit WrappedFLUID(recipient_, amount_);
  }
  
  function register(
    uint256 amount_
  ) external {
    require(amount_ > 0, "cannot register zero tokens");
    // rFLUID has 0 decimals, FLUID has 18 decimals, we disallow
    // trying to register fractions of FLUID tokens
    require(amount_ % 1e18 == 0, "can only register whole tokens");
    require(balanceOf(msg.sender) >= amount_, 'not enough tokens');
    _burn(msg.sender, amount_);
    rFLUID.onRegister(msg.sender, amount_ / 1e18);
    emit UnwrappedFLUID(msg.sender, amount_);
  }
  
  function batchTransfer(
    address[] calldata recipients_, 
    uint256[] calldata amounts_
  ) external {
    require(recipients_.length == amounts_.length, "recipients count differs from amounts");
    require(recipients_.length > 0, "batch is empty");
    for(uint256 i = 0; i < recipients_.length; i++) {
      _transfer(msg.sender, recipients_[i], amounts_[i]);
    }
  }
    
  function decimals() public pure override returns (uint8) {
    return DECIMALS_VALUE;
  }
}