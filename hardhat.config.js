require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
require("hardhat-gas-reporter");
require('@openzeppelin/hardhat-upgrades');
require("hardhat-watcher");
require('solidity-coverage');
require('dotenv').config()

module.exports = {
  networks: {
    arbitrum_testnet: {
      url: 'https://rinkeby.arbitrum.io/rpc',
      chanId: 421611,
      accounts: {
        mnemonic: process.env.ARBITRUM_TESTNET_MNEMONIC || '',
      },
    },
    hardhat: {
      initialBaseFeePerGas: 0 // hack needed to make solidity-coverage work on LONDON
    }
  },
  solidity: {
    version: "0.8.7",
    settings: {
      outputSelection: {
        "*": {
          "*": ["storageLayout"],
        },
      },
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
};

