const { expect } = require('chai');

const rFLUID_DECIMALS = 0
const rFLUID_NAME = 'Registered FLUID'
const rFLUID_SYMBOL = 'rFLUID'
const rFLUID_TOTAL_SUPPLY = 120_000_000

const FLUID_DECIMALS = 18
const FLUID_NAME = 'FluidFi'
const FLUID_SYMBOL = 'FLUID'

const ADDRESS_ZERO = '0x0000000000000000000000000000000000000000'

const BN = val => ethers.BigNumber.from(val)
const to18dec = val => BN(val).mul(BN(10).pow(BN(18)));

const getLatestBlockTimestamp = async () => {
  const latestBlock = await ethers.provider.getBlock(await ethers.provider.getBlockNumber());
  return latestBlock.timestamp;
}

const mineBlockAt = async timestamp => {
  if (typeof timestamp !== 'string') timestamp = timestamp.toString();
  timestamp = parseInt(timestamp, 10);
  await ethers.provider.send('evm_setNextBlockTimestamp', [timestamp]);
  await ethers.provider.send('evm_mine');
};

const expectRevert = async (fn, revertMsg) => {
  try {
    await fn;
  } catch (err) {
    if (err.message.includes(revertMsg)) return;
    console.log(err.message);
    throw new Error('incorrect error message');
  }
  throw new Error('should have thrown');
}

const WRAPPED_TOKENS = '0x000000000000000000000000000000000000FFff';

const DEFAULT_ADMIN_ROLE = '0x0000000000000000000000000000000000000000000000000000000000000000'
const TOKEN_UPDATER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('TOKEN_UPDATER_ROLE'))
const WHITELISTER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('WHITELISTER_ROLE'))

describe('rFLUID & FLUID', function() {

  let deployer, admin, tokenUpdater, whitelister, user, other, extra_user, addrs
  let rFLUID__Factory, FLUID__Factory
  
  before(async () => {
    rFLUID__Factory = await ethers.getContractFactory('rFLUID')
    FLUID__Factory = await ethers.getContractFactory('FLUID')
  })

  beforeEach(async () => {
    [deployer, admin, tokenUpdater, whitelister, founder, user, other, extra_user, ...addrs] = await ethers.getSigners()
  })
  
  describe('rFLUID', () => {
    describe('Token Specs', async () => {
      it('should have correct decimals', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.decimals()).to.equal(rFLUID_DECIMALS)
      })
      it('should have correct name', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.name()).to.equal(rFLUID_NAME)
      })
      it('should have correct symbol', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.symbol()).to.equal(rFLUID_SYMBOL)
      })
    })
    describe('Deployment', async () => {
      it('reverts if admin is address zero', async () => {
        await expect(
          upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, ADDRESS_ZERO, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address])
        ).to.be.revertedWith('admin_ is address zero')      
      })
      it('reverts if tokenUpdater is address zero', async () => {
        await expect(
          upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, ADDRESS_ZERO, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address])
        ).to.be.revertedWith('tokenUpdater_ is address zero')      
      })
      it('reverts if whitelister is address zero', async () => {
        await expect(
          upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, ADDRESS_ZERO, rFLUID_TOTAL_SUPPLY, founder.address])
        ).to.be.revertedWith('whitelister_ is address zero')      
      })
      it('reverts if initialSupply is zero', async () => {
        await expect(
          upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, 0, founder.address])
        ).to.be.revertedWith('initialSupply_ is zero')      
      })
      it('reverts if initialSupplyRecipient is address zero', async () => {
        await expect(
          upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, ADDRESS_ZERO])
        ).to.be.revertedWith('initialSupplyRecipient_ is address zero')      
      })
      it('should have correct admin', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.hasRole(DEFAULT_ADMIN_ROLE, admin.address)).to.equal(true)
      })
      it('should have only 1 admin', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.getRoleMemberCount(DEFAULT_ADMIN_ROLE)).to.equal(1)
      })
      it('should have correct tokenUpdater', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.hasRole(TOKEN_UPDATER_ROLE, tokenUpdater.address)).to.equal(true)
      })
      it('should have only 1 tokenUpdater', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.getRoleMemberCount(TOKEN_UPDATER_ROLE)).to.equal(1)
      })
      it('should have correct whitelister', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.hasRole(WHITELISTER_ROLE, whitelister.address)).to.equal(true)
      })
      it('should have only 1 whitelister', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.getRoleMemberCount(WHITELISTER_ROLE)).to.equal(1)
      })
      it('should add initialSupplyRecipient to whitelist', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.whitelist(founder.address)).to.equal(true)
      })
      it('should set totalSupply to initialSupply', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.totalSupply()).to.equal(BN(rFLUID_TOTAL_SUPPLY))
      })
      it('should mint initialSupply to initialSupplyRecipient', async () => {
        const rfluid = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        expect(await rfluid.balanceOf(founder.address)).to.equal(BN(rFLUID_TOTAL_SUPPLY))
      })
    })
    describe('setFLUID', async () => {
      beforeEach(async () => {
        this.rFLUID = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
      });
      it('reverts if caller does not have TOKEN_UPDATER_ROLE', async () => {
        await expectRevert(
          this.rFLUID.setFLUID(user.address),
          'missing role'
        );
      })
      it('reverts if address was already set', async () => {
        await this.rFLUID.connect(tokenUpdater).setFLUID(user.address)
        await expectRevert(
          this.rFLUID.connect(tokenUpdater).setFLUID(user.address),
          'FLUID already set'
        );
      });
      it('reverts if address is address zero', async () => {
        await expectRevert(
          this.rFLUID.connect(tokenUpdater).setFLUID(ADDRESS_ZERO),
          'FLUID_ is 0x0'
        );
      });
      it('sets FLUID variable + emits events', async () => {
        const tx = await this.rFLUID.connect(tokenUpdater).setFLUID(user.address)
        await expect(tx).to.emit(this.rFLUID, 'SetFLUID')
                        .withArgs(user.address)
        expect(await this.rFLUID.FLUID()).to.equal(user.address)
      })
    });
    describe('onRegister', async () => {
      beforeEach(async () => {
        this.rFLUID = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        await this.rFLUID.connect(tokenUpdater).setFLUID(other.address)
      });
      it('reverts if caller is not FLUID', async () => {
        await expectRevert(
          this.rFLUID.onRegister(user.address, 123),
          'caller is not FLUID'
        );
      })
      it('reverts if amount is zero', async () => {
        await expectRevert(
          this.rFLUID.connect(other).onRegister(user.address, 0),
          'cannot register zero'
        );
      });
      it('reverts if recipient is not whitelisted', async () => {
        await expectRevert(
          this.rFLUID.connect(other).onRegister(user.address, 123),
          'recipient not whitelisted'
        );
      });
    });
    describe('wrap', async () => {
      beforeEach(async () => {
        this.rFLUID = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
        this.FLUID = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
      });
      it('reverts if FLUID not set', async () => {
        await expectRevert(
          this.rFLUID.connect(user).wrap(123),
          'FLUID not set'
        );
      });
      it('reverts if sender is not whitelisted', async () => {
        await this.rFLUID.connect(tokenUpdater).setFLUID(this.FLUID.address)
        await this.rFLUID.connect(whitelister).removeFromWhitelist(founder.address)
        await expectRevert(
          this.rFLUID.connect(founder).wrap(123),
          'sender not whitelisted'
        );
      });
      it('reverts if amount is zero', async () => {
        await this.rFLUID.connect(tokenUpdater).setFLUID(this.FLUID.address)
        await this.rFLUID.connect(whitelister).addToWhitelist(user.address)
        await expectRevert(
          this.rFLUID.connect(user).wrap(0),
          'cannot wrap zero tokens'
        );
      });
      it('reverts if amount more than balance', async () => {
        await this.rFLUID.connect(tokenUpdater).setFLUID(this.FLUID.address)
        await this.rFLUID.connect(whitelister).addToWhitelist(user.address)
        await expectRevert(
          this.rFLUID.connect(user).wrap(123),
          'not enough tokens'
        );
      });
      it('emits correct events and updates rFLUID + FLUID balance', async () => {
        await this.rFLUID.connect(tokenUpdater).setFLUID(this.FLUID.address)
        await this.rFLUID.connect(whitelister).addToWhitelist(user.address)
        await this.rFLUID.connect(founder).transfer(user.address, 200)
        const tx = this.rFLUID.connect(user).wrap(50)
        await expect(tx).to.emit(this.rFLUID, 'Transfer')
                        .withArgs(user.address, WRAPPED_TOKENS, 50)
        await expect(tx).to.emit(this.rFLUID, 'UnregisteredRFLUID')
                        .withArgs(user.address, 50)
        await expect(tx).to.emit(this.FLUID, 'Transfer')
                        .withArgs(ADDRESS_ZERO, user.address, to18dec(50))
        await expect(tx).to.emit(this.FLUID, 'WrappedFLUID')
                        .withArgs(user.address, to18dec(50))
        expect((await this.rFLUID.balanceOf(user.address)).toString()).to.equal(BN(150))
        expect((await this.FLUID.balanceOf(user.address)).toString()).to.equal(to18dec(50))
      })
    });
    
    describe('Whitelisting', async () => {
      beforeEach(async () => {
        this.rFLUID = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
      });
      describe('addToWhitelist', async () => {
        it('reverts if caller does not have WHITELISTER_ROLE', async () => {
          await expectRevert(
            this.rFLUID.connect(other).addToWhitelist(user.address),
            'missing role'
          )
        })
        it('reverts if account is address zero', async () => {
          await expect(
            this.rFLUID.connect(whitelister).addToWhitelist(ADDRESS_ZERO)
          ).to.be.revertedWith('cannot whitelist address zero')
        })
        it('reverts if account already whitelisted', async () => {
          await this.rFLUID.connect(whitelister).addToWhitelist(user.address)
          await expect(
            this.rFLUID.connect(whitelister).addToWhitelist(user.address)
          ).to.be.revertedWith('already whitelisted')
        })
        it('adds account to whitelist and emits event', async () => {
          const tx = this.rFLUID.connect(whitelister).addToWhitelist(user.address)
          await expect(tx).to.emit(this.rFLUID, 'WhitelistAdded')
                          .withArgs(user.address)
          expect(await this.rFLUID.whitelist(user.address)).to.equal(true)
        })
      })
      describe('removeFromWhitelist', async () => {
        it('reverts if caller does not have WHITELISTER_ROLE', async () => {
          await expectRevert(
            this.rFLUID.connect(other).removeFromWhitelist(user.address),
            'missing role'
          )
        })
        it('reverts if account not in whitelist', async () => {
          await expect(
            this.rFLUID.connect(whitelister).removeFromWhitelist(user.address)
          ).to.be.revertedWith('not in whitelist')
        })
        it('removes account from whitelist and emits event', async () => {
          await this.rFLUID.connect(whitelister).addToWhitelist(user.address)
          const tx = this.rFLUID.connect(whitelister).removeFromWhitelist(user.address)
          await expect(tx).to.emit(this.rFLUID, 'WhitelistRemoved')
                          .withArgs(user.address)
          expect(await this.rFLUID.whitelist(user.address)).to.equal(false)
        })
      })
      describe('batchAddToWhitelist', async () => {
        it('reverts if caller does not have WHITELISTER_ROLE', async () => {
          await expectRevert(
            this.rFLUID.connect(other).batchAddToWhitelist([user.address]),
            'missing role'
          )
        })
        it('adds accounts to whitelist and emits events', async () => {
          const tx = this.rFLUID.connect(whitelister).batchAddToWhitelist([user.address, addrs[8].address])
          await expect(tx).to.emit(this.rFLUID, 'WhitelistAdded')
                          .withArgs(user.address)
          await expect(tx).to.emit(this.rFLUID, 'WhitelistAdded')
                          .withArgs(addrs[8].address)
          expect(await this.rFLUID.whitelist(user.address)).to.equal(true)
          expect(await this.rFLUID.whitelist(addrs[8].address)).to.equal(true)
        })        
      })
    })
  });
  
  describe('FLUID', () => {
    beforeEach(async () => {
      this.rFLUID = await upgrades.deployProxy(rFLUID__Factory, [rFLUID_NAME, rFLUID_SYMBOL, admin.address, tokenUpdater.address, whitelister.address, rFLUID_TOTAL_SUPPLY, founder.address]);
    })
    describe('Token Specs', async () => {
      it('should have correct decimals', async () => {
        const fluid = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        expect(await fluid.decimals()).to.equal(FLUID_DECIMALS)
      })
      it('should have correct name', async () => {
        const fluid = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        expect(await fluid.name()).to.equal(FLUID_NAME)
      })
      it('should have correct symbol', async () => {
        const fluid = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        expect(await fluid.symbol()).to.equal(FLUID_SYMBOL)
      })
    })

    describe('Deployment', async () => {
      it('reverts if admin is address zero', async () => {
        await expect(
          upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, ADDRESS_ZERO, this.rFLUID.address])
        ).to.be.revertedWith('admin_ is address zero')      
      })
      it('reverts if rFLUID is address zero', async () => {
        await expect(
          upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, ADDRESS_ZERO])
        ).to.be.revertedWith('rFLUID_ is address zero')      
      })
      it('should have correct admin', async () => {
        const fluid = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        expect(await fluid.hasRole(DEFAULT_ADMIN_ROLE, admin.address)).to.equal(true)
      })
      it('should have only 1 admin', async () => {
        const fluid = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        expect(await fluid.getRoleMemberCount(DEFAULT_ADMIN_ROLE)).to.equal(1)
      })
      it('should set rFLUID contract address', async () => {
        const fluid = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        expect(await fluid.rFLUID()).to.equal(this.rFLUID.address)
      })
      it('should NOT have an initial total supply', async () => {
        const fluid = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        expect(await fluid.totalSupply()).to.equal(0)
      })
    })
    describe('onWrap', async () => {
      it('reverts if caller is not rFLUID', async () => {
        const FLUID = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        await expectRevert(
          FLUID.onWrap(user.address, 123),
          'caller is not rFLUID'
        );
      })
      it('reverts if recipient is address zero', async () => {
        const FLUID = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, other.address]);
        await expectRevert(
          FLUID.connect(other).onWrap(ADDRESS_ZERO, 1),
          'cannot wrap to address zero'
        );
      });
      it('reverts if amount is zero', async () => {
        const FLUID = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, other.address]);
        await expectRevert(
          FLUID.connect(other).onWrap(user.address, 0),
          'cannot wrap zero tokens'
        );
      });
    })
    describe('register', async () => {
      beforeEach(async () => {
        this.FLUID = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        await this.rFLUID.connect(tokenUpdater).setFLUID(this.FLUID.address)
        await this.rFLUID.connect(founder).wrap(100)
        await this.FLUID.connect(founder).transfer(user.address, to18dec(100))
      });
      it('reverts if amount is zero', async () => {
        await expectRevert(
          this.FLUID.connect(user).register(0),
          'cannot register zero tokens'
        );
      });
      it('reverts if amount not a whole token', async () => {
        await expectRevert(
          this.FLUID.connect(user).register(to18dec(100).sub(1)),
          'can only register whole tokens'
        );
      });
      it('reverts if amount more than balance', async () => {
        await expectRevert(
          this.FLUID.connect(user).register(to18dec(101)),
          'not enough tokens'
        );
      });
      it('reverts if caller not whitelisted in rFLUID', async () => {
        await expectRevert(
          this.FLUID.connect(user).register(to18dec(1)),
          'recipient not whitelisted'
        );
      });
      it('emits correct events and updates rFLUID + FLUID balance', async () => {
        await this.rFLUID.connect(whitelister).addToWhitelist(user.address)
        const tx = this.FLUID.connect(user).register(to18dec(25))
        await expect(tx).to.emit(this.FLUID, 'Transfer')
                        .withArgs(user.address, ADDRESS_ZERO, to18dec(25))
        await expect(tx).to.emit(this.FLUID, 'UnwrappedFLUID')
                        .withArgs(user.address, to18dec(25))
        await expect(tx).to.emit(this.rFLUID, 'RegisteredRFLUID')
                        .withArgs(user.address, 25)
        await expect(tx).to.emit(this.rFLUID, 'Transfer')
                        .withArgs(WRAPPED_TOKENS, user.address, 25)
        expect((await this.rFLUID.balanceOf(user.address)).toString()).to.equal(BN(25))
        expect((await this.FLUID.balanceOf(user.address)).toString()).to.equal(to18dec(75))
      })
    })
    describe('batchTransfer', async () => {
      beforeEach(async () => {
        this.FLUID = await upgrades.deployProxy(FLUID__Factory, [FLUID_NAME, FLUID_SYMBOL, admin.address, this.rFLUID.address]);
        await this.rFLUID.connect(tokenUpdater).setFLUID(this.FLUID.address)
        await this.rFLUID.connect(founder).wrap(1)      
      });
      it('reverts if recipients list bigger then amounts', async () => {
        await expectRevert(
          this.FLUID.connect(other).batchTransfer([user.address, extra_user.address], [1]),
          'recipients count differs from amounts'
        )
      })
      it('reverts if amounts list bigger then recipients', async () => {
        await expectRevert(
          this.FLUID.connect(other).batchTransfer([user.address], [1, 2]),
          'recipients count differs from amounts'
        )
      })
      it('reverts if recipients and amounts lists are empty', async () => {
        await expectRevert(
          this.FLUID.connect(other).batchTransfer([], []),
          'batch is empty'
        )
      })
      it('succeeds otherwise', async () => {
        await this.FLUID.connect(admin).grantRole(DEFAULT_ADMIN_ROLE, founder.address)
        await this.FLUID.connect(founder).transfer(other.address, to18dec(1))
        await this.FLUID.connect(other).batchTransfer([user.address], [1])  
      })
    });        
  })
})